### About
This application fetches tweets and aggregates them to display number of tweets, tweeted in given hours of the day. Obtained tweets are the most recent from last X hours, where X can be set by LAST_HOURS variable. Batch size of tweets per request can be set as well with TWEETS_BATCH_SIZE variable.

### Reqiurements
- Docker version 18.03.1
- docker-compose version 1.19.0

### Running
`docker-compose up -d`

After first run:

- `docker exec -w /var/www/finder/app/ app composer install -q -n`

- Add valid TWITTER_API_KEY and TWITTER_API_SECRET to environment or to .env file

### Testing
`docker exec -w /var/www/finder/app/ app ./vendor/bin/codecept run`

### Linter
`docker exec -w /var/www/finder/app/ app php vendor/bin/phpcs --standard=PSR2 --ignore=vendor,src/console.php -w --colors src/`

### Shortages
Due to the lack of time I didn't implement gitlab-ci and logging
