<?php

namespace App\Client;

use Freebird\Services\freebird\Client;

class TwitterClient
{
    const USER_TIMELINE_ENDPOINT = 'statuses/user_timeline.json';
    const USER_NOT_FOUND_CODE = 34;

    /** @var Client */
    private $freebirdClient;

    /** @var int */
    private $tweetsBatchSize;

    public function __construct(Client $freebirdClient, int $tweetsBatchSize)
    {
        $this->freebirdClient = $freebirdClient;
        $this->tweetsBatchSize = $tweetsBatchSize;
    }

    public function fetchTweets(string $username, int $maxId = null): array
    {
        $parameters = [
            'screen_name' => $username,
            'count' => $this->tweetsBatchSize,
            'trim_user' => true
        ];

        if ($maxId !== null) {
            $parameters['max_id'] = $maxId;
        }

        $responseString = $this->freebirdClient->api_request(self::USER_TIMELINE_ENDPOINT, $parameters);
        $response = json_decode($responseString, true);

        if (!isset($response['errors'])) {
            return $response;
        }

        $this->handleErrors($response);
    }

    private function handleErrors(array $response)
    {
        foreach ($response['errors'] as $error) {
            if ($error['code'] === self::USER_NOT_FOUND_CODE) {
                throw new UserNotFoundException();
            }
        }

        throw new TwitterApiException();
    }
}
