<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;

class NameController
{
    public function default(): Response
    {
        return new Response('Try /hello/:name');
    }

    public function name(string $name): Response
    {
        return new Response('Hello ' . $name);
    }
}
