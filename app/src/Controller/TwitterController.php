<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use App\Model\TwitterModel;
use App\Client\UserNotFoundException;
use App\Client\TwitterApiException;

class TwitterController
{
    public function histogram(string $username, TwitterModel $twitterModel): JsonResponse
    {
        try {
            $histogram = $twitterModel->getHistogram($username);
        } catch (UserNotFoundException $e) {
            return new JsonResponse(['error' => 'Requested user does not exist'], 404);
        } catch (TwitterApiException $e) {
            return new JsonResponse(['error' => 'Could not get tweets from twitter'], 500);
        } catch (\Exception $e) {
            return new JsonResponse(['error' => 'Internal server error'], 500);
        }

        return new JsonResponse($histogram);
    }
}
