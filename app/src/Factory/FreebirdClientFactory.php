<?php

namespace App\Factory;

use Freebird\Services\freebird\Client;

class FreebirdClientFactory
{
    public static function create(string $apiKey, string $apiSecret)
    {
        $freebirdClient = new Client();
        $freebirdClient->init_bearer_token($apiKey, $apiSecret);

        return $freebirdClient;
    }
}
