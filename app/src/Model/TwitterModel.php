<?php

namespace App\Model;

use App\Client\TwitterClient;
use App\Tool\TweetsAggregator;
use App\Tool\TweetsFilter;

class TwitterModel
{
    /** @var TwitterClient */
    private $twitterClient;

    /** @var int */
    private $tweetsBatchSize;

    /** @var string */
    private $dateModifier;

    public function __construct(TwitterClient $twitterClient, int $lastHours, int $tweetsBatchSize)
    {
        $this->twitterClient = $twitterClient;
        $this->tweetsBatchSize = $tweetsBatchSize;
        $this->dateModifier = sprintf('-%d hour', $lastHours);
    }

    public function getHistogram(string $username): array
    {
        $tweets = $this->fetchTweets($username);
        return TweetsAggregator::aggregateTweets($tweets);
    }

    private function fetchTweets(string $username): array
    {
        $filteredTweets = [];
        $maxId = null;
        $tweetsSinceDate = new \DateTime('now');
        $tweetsSinceDate->modify('last sec');
        $tweetsSinceDate->modify($this->dateModifier);

        do {
            $tweets = $this->twitterClient->fetchTweets($username, $maxId);

            if ($maxId !== null) {
                unset($tweets[0]);
            }

            $maxId = $this->getMaxId($tweets);

            list($currentTweets, $moreToProcess) = TweetsFilter::filterTweetsSinceDate(
                $tweets,
                $tweetsSinceDate,
                $this->tweetsBatchSize
            );

            $filteredTweets = array_merge($filteredTweets, $currentTweets);
        } while ($moreToProcess);

        return $filteredTweets;
    }

    private function getMaxId(array $tweets): ?int
    {
        if (empty($tweets)) {
            return null;
        }

        $lastTweet = end($tweets);
        return $lastTweet['id'];
    }
}
