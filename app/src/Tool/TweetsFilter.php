<?php

namespace App\Tool;

class TweetsFilter
{
    public static function filterTweetsSinceDate(array $tweets, \DateTime $date, int $tweetsBatchSize): array
    {
        $filteredTweets = [];
        $moreToProcess = true;
        if (empty($tweets) || count($tweets) < $tweetsBatchSize) {
            $moreToProcess = false;
        }

        foreach ($tweets as $tweet) {
            $createdAt = new \DateTime($tweet['created_at']);
            
            if ($createdAt < $date) {
                $moreToProcess = false;
                break;
            }

            $filteredTweets[] = $tweet;
        }

        return [$filteredTweets, $moreToProcess];
    }
}
