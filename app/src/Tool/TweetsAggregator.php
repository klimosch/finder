<?php

namespace App\Tool;

class TweetsAggregator
{
    public static function aggregateTweets(array $tweets): array
    {
        $aggregatedTweets = array_fill(0, 24, 0);

        foreach ($tweets as $tweet) {
            $createdAt = new \DateTime($tweet['created_at']);
            $creationHour = $createdAt->format('G');

            $aggregatedTweets[$creationHour]++;
        }

        return $aggregatedTweets;
    }
}
