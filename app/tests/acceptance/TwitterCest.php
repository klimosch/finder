<?php

use PHPUnit\Framework\Assert;

class TwitterCest
{
    public function testGetHistogramAction(AcceptanceTester $I)
    {
        $I->sendGET('/histogram/Ferrari');

        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
        
        $response = $I->grabDataFromResponseByJsonPath('$.');
        $response = $response[0];
        Assert::AssertCount(24, $response);
    }
}
