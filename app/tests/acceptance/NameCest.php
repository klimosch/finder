<?php

use PHPUnit\Framework\Assert;

class NameCest
{
    public function testDefaultAction(AcceptanceTester $I)
    {
        $I->sendGET('');

        $I->seeResponseCodeIs(200);
        $response = $I->grabResponse();
        Assert::AssertEquals('Try /hello/:name', $response);
    }

    public function testNameAction(AcceptanceTester $I)
    {
        $I->sendGET('/hello/Bob');

        $I->seeResponseCodeIs(200);
        $response = $I->grabResponse();
        Assert::AssertEquals('Hello Bob', $response);
    }
}
