<?php

namespace App\Client;

use phpunit\Framework\TestCase;
use Freebird\Services\freebird\Client;

class TwitterClientTest extends TestCase
{
    /** @var TwitterClient */
    private $twitterClient;

    /** @var Client */
    private $freebirdClientMock;

    /** @var int */
    private $tweetsBatchSize;

    /** @var string */
    private $username;

    /** @var array */
    private $expectedParameters;

    public function setUp()
    {
        $this->tweetsBatchSize = 5;
        $this->username = 'bob';
        $this->expectedParameters = [
            'screen_name' => $this->username, 
            'count' => $this->tweetsBatchSize,
            'trim_user' => true
        ];

        $this->freebirdClientMock = $this->getMockBuilder(Client::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->twitterClient = new TwitterClient($this->freebirdClientMock, $this->tweetsBatchSize);
    }

    public function testFetchTweets()
    {
        $tweets = [
            ['id' => 1],
            ['id' => 2]
        ];

        $this->freebirdClientMock
            ->expects($this->once())
            ->method('api_request')
            ->with(TwitterClient::USER_TIMELINE_ENDPOINT, $this->expectedParameters)
            ->willReturn(json_encode($tweets));
        
        $response = $this->twitterClient->fetchTweets($this->username);

        self::AssertEquals($tweets, $response);
    }

    public function testFetchTweetsWithNoTweets()
    {
        $this->expectedParameters['max_id'] = 1;

        $this->freebirdClientMock
            ->expects($this->once())
            ->method('api_request')
            ->with(TwitterClient::USER_TIMELINE_ENDPOINT, $this->expectedParameters)
            ->willReturn(json_encode([]));
        
        $response = $this->twitterClient->fetchTweets($this->username, 1);

        self::AssertEquals([], $response);
    }

    /**
     * @expectedException App\Client\UserNotFoundException
     */
    public function testFetchTweetsForNonExistingUser()
    {
        $this->freebirdClientMock
            ->expects($this->once())
            ->method('api_request')
            ->with(TwitterClient::USER_TIMELINE_ENDPOINT, $this->expectedParameters)
            ->willReturn(
                json_encode(
                    [
                        'errors' => [
                            ['code' => TwitterClient::USER_NOT_FOUND_CODE]
                        ]
                    ]
                )
            );
        
        $response = $this->twitterClient->fetchTweets($this->username);
    }

    /**
     * @expectedException App\Client\TwitterApiException
     */
    public function testFetchTweetsWithErrors()
    {
        $this->freebirdClientMock
            ->expects($this->once())
            ->method('api_request')
            ->with(TwitterClient::USER_TIMELINE_ENDPOINT, $this->expectedParameters)
            ->willReturn(
                json_encode(
                    [
                        'errors' => [
                            ['code' => 40]
                        ]
                    ]
                )
            );
        
        $response = $this->twitterClient->fetchTweets($this->username);
    }
}
