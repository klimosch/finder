<?php

namespace App\Model;

use phpunit\Framework\TestCase;
use App\Client\TwitterClient;
use App\Client\TwitterApiException;

class TwitterModelTest extends TestCase
{
    /** @var TwitterModel */
    private $twitterModel;

    /** @var TwitterClient */
    private $twitterClientMock;

    /** @var \DateTime */
    private $tweetsSinceDate;

    /** @var string */
    private $username;

    public function setUp()
    {
        $lastHours = 24;
        $tweetsBatchSize = 2;
        $this->username = 'bob';
        $this->tweetsSinceDate = new \DateTime('now');

        $this->twitterClientMock = $this->getMockBuilder(TwitterClient::class)
            ->disableOriginalConstructor()
            ->getMock();
        
        $this->twitterModel = new TwitterModel($this->twitterClientMock, $lastHours, $tweetsBatchSize);
    }

    public function testGetHisotgramWithoutTweets()
    {
        $this->twitterClientMock
            ->expects($this->once())
            ->method('fetchTweets')
            ->with($this->username, null)
            ->willReturn([]);
        
        $aggregatedTweets = $this->twitterModel->getHistogram($this->username);
        
        self::AssertCount(24, $aggregatedTweets);
        foreach ($aggregatedTweets as $tweetsNumber) {
            self::AssertEquals(0, $tweetsNumber);
        }
    }

    /**
     * @expectedException App\Client\TwitterApiException
     */
    public function testGetHistogramWithErrors()
    {
        $this->twitterClientMock
            ->expects($this->once())
            ->method('fetchTweets')
            ->with($this->username, null)
            ->willThrowException(new TwitterApiException());

        $this->twitterModel->getHistogram($this->username);
    }

    public function testGetHistogramInOneBatch()
    {
        $tweets = [
            [
                'created_at' => $this->tweetsSinceDate->modify('-1 hour')->format('D M d H:i:s O Y'),
                'id' => 1
            ]
        ];
        $tweetHour = $this->tweetsSinceDate->format('G');

        $this->twitterClientMock
            ->expects($this->once())
            ->method('fetchTweets')
            ->with($this->username, null)
            ->willReturn($tweets);

        $aggregatedTweets = $this->twitterModel->getHistogram($this->username);

        self::AssertCount(24, $aggregatedTweets);
        foreach ($aggregatedTweets as $hour => $tweetsNumber) {
            if ($hour == $tweetHour) {
                self::AssertEquals(1, $tweetsNumber);
                continue;
            }
            self::AssertEquals(0, $tweetsNumber);
        }
    }

    public function testGetHistogramInTwoBatches()
    {
        $tweetsBatch1 = [
            [
                'created_at' => $this->tweetsSinceDate->modify('-1 hour')->format('D M d H:i:s O Y'),
                'id' => 1
            ],
            [
                'created_at' => $this->tweetsSinceDate->modify('-1 hour')->format('D M d H:i:s O Y'),
                'id' => 2
            ]
        ];
        $tweetsBatch2 = [
            [
                'created_at' => $this->tweetsSinceDate->format('D M d H:i:s O Y'),
                'id' => 2
            ],
            [
                'created_at' => $this->tweetsSinceDate->modify('-1 hour')->format('D M d H:i:s O Y'),
                'id' => 3
            ]
        ];

        $this->twitterClientMock
            ->expects($this->at(0))
            ->method('fetchTweets')
            ->with($this->username, null)
            ->willReturn($tweetsBatch1);
        
        $this->twitterClientMock
            ->expects($this->at(1))
            ->method('fetchTweets')
            ->with($this->username, 2)
            ->willReturn($tweetsBatch2);

        $aggregatedTweets = $this->twitterModel->getHistogram($this->username);

        self::AssertCount(24, $aggregatedTweets);
        self::AssertEquals(3, array_sum($aggregatedTweets));
    }
}
