<?php

namespace App\Tool;

use phpunit\Framework\TestCase;

class TweetsAggregatorTest extends TestCase
{
    public function testAggregateTweetsWithoutTweets()
    {
        $aggregatedTweets = TweetsAggregator::aggregateTweets([]);

        self::AssertCount(24, $aggregatedTweets);
        foreach ($aggregatedTweets as $tweetsNumber) {
            self::AssertEquals(0, $tweetsNumber);
        }
    }

    public function testAggregateTweets()
    {
        $tweets = [
            [
                'created_at' => (new \DateTime('Thu Apr 06 15:24:15 +0000 2017'))->format('D M d H:i:s O Y'),
            ],
            [
                'created_at' => (new \DateTime('Thu Apr 06 15:28:15 +0000 2017'))->format('D M d H:i:s O Y'),
            ],
            [
                'created_at' => (new \DateTime('Thu Apr 05 14:24:15 +0000 2017'))->format('D M d H:i:s O Y'),
            ],
            [
                'created_at' => (new \DateTime('Thu Apr 04 14:24:15 +0000 2017'))->format('D M d H:i:s O Y'),
            ]
        ];

        $aggregatedTweets = TweetsAggregator::aggregateTweets($tweets);
        self::AssertCount(24, $aggregatedTweets);
        foreach ($aggregatedTweets as $hour => $tweetsNumber) {
            if ($hour === 15 || $hour === 14) {
                self::AssertEquals(2, $tweetsNumber);
                continue;
            }
            self::AssertEquals(0, $tweetsNumber);
        }
    }
}
