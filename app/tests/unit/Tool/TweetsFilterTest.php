<?php

namespace App\Tool;

use phpunit\Framework\TestCase;

class TweetsFilterTest extends TestCase
{
    /** @var \DateTime */
    private $tweetsSinceDate;

    /** @var array */
    private $tweets;

    public function setUp()
    {
        $this->tweetsSinceDate = new \DateTime('now');

        $this->tweets = [
            [
                'created_at' => $this->tweetsSinceDate->modify('-1 hour')->format('D M d H:i:s O Y'),
            ],
            [
                'created_at' => $this->tweetsSinceDate->modify('-5 hour')->format('D M d H:i:s O Y'),
            ],
            [
                'created_at' => $this->tweetsSinceDate->modify('-2 minute')->format('D M d H:i:s O Y'),
            ],
        ];
    }

    public function testFilterTweetsSinceDateWithoutTweets()
    {
        list($filteredTweets, $moreToProcess) = TweetsFilter::filterTweetsSinceDate([], $this->tweetsSinceDate, 10);

        self::AssertFalse($moreToProcess);
        self::AssertCount(0, $filteredTweets);
    }

    public function testFilterTweetsSinceDateWithNoMoreProcessing()
    {
        $this->tweetsSinceDate->modify('+1 minute');

        list($filteredTweets, $moreToProcess) = TweetsFilter::filterTweetsSinceDate($this->tweets, $this->tweetsSinceDate, 10);

        self::AssertFalse($moreToProcess);
        self::AssertCount(2, $filteredTweets);
    }

    public function testFilterTweetsSinceDateWithMoreProcessing()
    {
        $this->tweetsSinceDate->modify('-1 minute');

        list($filteredTweets, $moreToProcess) = TweetsFilter::filterTweetsSinceDate($this->tweets, $this->tweetsSinceDate, 2);

        self::AssertTrue($moreToProcess);
        self::AssertCount(3, $filteredTweets);
    }
}
